// Esto era lo que el profe indico para recuperar las imágenes, pero me provoca fallas en mis pruebas:

// export const heroImages = require.context('../../public/assets/heroes', true );

// Debido a problemas de Webpack con Jest

// Así que esta es un solución:

let heroImages = () => ({ default:''});
let brandsImages = () => ({ default:''});

try{
    heroImages = require.context('../../public/assets/heroes', true );
    brandsImages = require.context('../../public/assets/heroes', true );
}catch(error){}

export const loadImage = image => (heroImages(`./${image}`).default );
export const loadBrand = brand => (brandsImages(`./${brand}`).default);






