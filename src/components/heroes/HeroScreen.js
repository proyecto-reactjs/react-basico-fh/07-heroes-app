import React, { useMemo } from 'react';
import { useParams, Redirect } from 'react-router-dom';
import { loadImage } from '../../helpers/heroImages';
import { getHeroById } from '../../selectors/getHeroById';

export const HeroScreen = ({ history }) => {

    const { heroeId } = useParams();

    const hero = useMemo(() => getHeroById( heroeId ), [ heroeId ]);

    if ( !hero ) {
        return <Redirect to="/" />;
    }

    const handleReturn = () => {

        if( history.length <=2 ) {
            history.push('/');
        } else {
            history.goBack();
        }

    }

    const {
        superhero,
        publisher,
        alter_ego,
        first_appearance,
        characters,
    } = hero;
    
    return (
        <div className="row mt-5">
            <div className="col-12 col-sm-4 mb-4 mb-sm-0">
                <img 
                    src={ loadImage(`${ heroeId }.jpg`)}
                    //  {heroImages(`./${ heroeId }.jpg`).default}, esto era antes, pero se hizo modificaciones para que el test funcione con require.context
                    alt={ superhero }
                    className="img-thumbnail animate__animated animate__fadeInLeft"
                />
            </div>

            <div className="col-12 col-sm-8 mb-4 mb-sm-0 animate__animated animate__fadeIn">
                <h3> { superhero } </h3>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item"> <b> Alter ego: </b> { alter_ego } </li>
                    <li className="list-group-item"> <b> Publisher: </b> { publisher } </li>
                    <li className="list-group-item"> <b> First appearance: </b> { first_appearance } </li>
                </ul>

                <h5> Characters </h5>
                <p> { characters } </p>

                <button 
                    className="btn btn-outline-info w-100"
                    onClick={ handleReturn }
                >
                    Return
                </button>

            </div>

        </div>
    )
}
