import React, { useContext } from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';
import { AuthContext } from '../../auth/AuthContext';
import { types } from '../../types/types';

export const Navbar = () => {

    const { user:{ name }, dispatch } = useContext(AuthContext);
    const history = useHistory();

    const handleLogout = () => {

        history.replace('/login');

        dispatch({
            type: types.logout
        });
    }

    // const hidden =() =>{

    //     const menu=document.querySelector("#navbarTogglerDemo03");
    //     menu.classList.toggle('collapse')
    //     onClick={hidden}, para lanzarlo con el button
    // }

    // data-bs-toggle="collapse" no funciono y quise usar lo comentado arriba,pero con data-bs-toggle="collapse" me funciono

    return (

        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                 
            <div className="container-fluid">
                <Link
                    className="navbar-brand "
                    to="/"
                >
                    Asociaciones
                </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"  aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-between" id="navbarTogglerDemo03">
                    <div className="navbar-nav">
                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link" //d-flex justify-content-center, si es que quiero centrar
                            exact
                            to="/marvel"
                        >
                            Marvel
                        </NavLink>
                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/dc"
                        >
                            DC
                        </NavLink>
                
                        <NavLink
                            activeClassName="active"
                            className="nav-item nav-link"
                            exact
                            to="/search"
                        >
                            Search
                        </NavLink>
                    </div>
                    <ul className="navbar-nav ml-auto">
                        <span className="nav-item nav-link text-info">
                            { name }
                        </span>
                        <button
                            className="nav-item nav-link btn d-flex"
                            onClick={ handleLogout }
                        >
                            Logout
                        </button>
                    </ul>
                </div>
            </div>
{/* 
            <div className="collapse navbar-collapse  w-100 order-3 dual-collapse2">

            </div> */}
        </nav>
    )
}