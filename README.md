# Curso React: De cero a experto ( Hooks y MERN )

## Proyecto Heroes App

Instruido por:

Fernando Herrera

![Portada](https://i.imgur.com/9oZf50r.png)

## Nota:

creado con:

`npx create-react-app`

### Comandos:

- `npm install query-string`
- `npm install --save react-router-dom`
- `npm install --save-dev enzyme`
- `npm install --save-dev @wojtekmaj/enzyme-adapter-react-17`
- `npm install --save-dev enzyme-to-json`

<!-- ### Para usar Gitlab pages:


- #### Basename que esta en AppRouter:

  - `<Router basename= "/react-basico-fh/07-heroes-app/" >`
  - `/react-basico-fh/07-heroes-app/` reemplazarlo por tu Url base

####

- #### Colocarlo en el package.json:

  - `"homepage": "https://proyecto-reactjs.gitlab.io/react-basico-fh/07-heroes-app/"`,
  - `/react-basico-fh/07-heroes-app/` reemplazarlo por tu Url base

####

- #### Esta documentación me ayudo:

  (https://create-react-app.dev/docs/deployment/#building-for-relative-paths) -->
